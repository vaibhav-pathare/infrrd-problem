# README #

Develop a system that handles a large amount of string matching requests, which may
include bad regexes as input. How would you exit the backtracking limbo?

### What is this repository for? ###

* Quick summary: 
* Version: v0

### How do I get set up? ###

* Summary of set up
1. Copy problem_api directory at /etc/infrrd/config/

* Deployment instructions
1. Copy problems.war file into your tomcat webapps directory.
2. Check the log at /tmp/api

* How to run tests
curl --location --request POST 'localhost:8080/problems/v0/api/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "regex":"\\bcat\\b",
    "textBody":"cat cat cat cattie cat"
}'

