package com.infrrd.api.listener;

import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;

/**
 * INIT THE CONFIGURATION
 * 
 * @author Vaibhav Pathare
 *
 */
public class ContextListener implements ServletContextListener {
	public static Logger mainLog = LogManager.getLogger("main");
	public static Logger accessLog = LogManager.getLogger("access");
	public static Logger errorLog = LogManager.getLogger("error");

	/**
	 * DESTROY CONFIGURATION
	 */
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		mainLog.info("Undeploying problem web application");
		// ----------------------------------------------------------------

		mainLog.info("problem web application undeployed successfully");
	}

	/**
	 * INIT CONFIGURATION
	 */
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		System.out.println("Deploying Live Chat web application...");
		// ----------------------------------------------------------------

		// get log file path
		String logFile = servletContextEvent.getServletContext()
				.getInitParameter("logFile");
		System.out.println("logFile:" + logFile);

		// get configuration file path
		String configFile = servletContextEvent.getServletContext()
				.getInitParameter("config");
		System.out.println("configFile:" + configFile);

		LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) LogManager
				.getContext(false);
		File file = new File(logFile);
		context.setConfigLocation(file.toURI());
		mainLog.info("problem web application deployed successfully");
		// ----------------------------------------------------------------
	}
}