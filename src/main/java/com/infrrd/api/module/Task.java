package com.infrrd.api.module;

import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Task implements Callable<String> {
	public static Logger mainLog = LogManager.getLogger("main");
	public static Logger accessLog = LogManager.getLogger("access");
	public static Logger errorLog = LogManager.getLogger("error");

	private Pattern pattern;
	private String text;

	public Task(Pattern pattern, String text) {
		this.pattern = pattern;
		this.text = text;
	}

	public Pattern getPattern() {
		return pattern;
	}

	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String call() throws Exception {
		// match the pattern
		Matcher matcher = pattern.matcher(text);
		// ----------------------------------------------------------------

		// get the match
		String match = null;
		while (matcher.find()) {
			match = matcher.group(0);
			break;
		}
		return match;
	}
}