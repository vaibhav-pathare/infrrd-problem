package com.infrrd.api.request;

/**
 * API Request
 * 
 * @author Vaibhav Pathare
 *
 */
public class APIRequest {
	private String regex;
	private String textBody;

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public String getTextBody() {
		return textBody;
	}

	public void setTextBody(String textBody) {
		this.textBody = textBody;
	}

	public boolean validate() {
		if (null == regex || regex.isEmpty()) {
			return false;
		}

		if (null == textBody || textBody.isEmpty()) {
			return false;
		}
		return true;
	}
}
