package com.infrrd.api;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.infrrd.api.module.Task;
import com.infrrd.api.request.APIRequest;
import com.infrrd.api.response.APIResponse;

/**
 * API
 * 
 * @author Vaibhav Pathare
 *
 */

@Path("/api/")
public class API {
	public static Logger mainLog = LogManager.getLogger("main");
	public static Logger accessLog = LogManager.getLogger("access");
	public static Logger errorLog = LogManager.getLogger("error");

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response ocr(@Context HttpServletRequest request,
			@Context HttpServletResponse response, APIRequest payload) {
		accessLog.info("Incoming request[POST]: Match text");
		// ----------------------------------------------------------------

		// validate the payload
		if (!payload.validate()) {
			String json = new APIResponse(null, true).toJson();
			return Response.status(200).entity(json).build();
		}
		accessLog.debug("regex:" + payload.getRegex());
		accessLog.debug("text body:" + payload.getTextBody());
		// ----------------------------------------------------------------

		// compile the pattern
		Pattern pattern = null;
		try {
			pattern = Pattern.compile(payload.getRegex());
		} catch (PatternSyntaxException e) {
			accessLog.error("error while compiling pattern:" + e);
			String json = new APIResponse(null, true).toJson();
			return Response.status(200).entity(json).build();
		}
		String text = payload.getTextBody();
		// ----------------------------------------------------------------

		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<String> future = executor.submit(new Task(pattern, text));
		
		//timeout for matcher
		int timeout = 5;
		
		boolean error = false;
		String match = null;
		try {
			accessLog.debug("Matcher Started..");
			match = future.get(timeout, TimeUnit.SECONDS);
			accessLog.debug("Matcher Finished!");
		} catch (TimeoutException e) {
			error = true;
			future.cancel(true);
			accessLog.warn("Matcher Terminated!");
		} catch (InterruptedException e) {
			error = true;
			future.cancel(true);
			accessLog.error("Matcher InterruptedException:" + e);
			e.printStackTrace();
		} catch (ExecutionException e) {
			error = true;
			future.cancel(true);
			accessLog.error("Matcher ExecutionException:" + e);
			e.printStackTrace();
		} catch (Throwable e) {
			error = true;
			future.cancel(true);
			accessLog.error("Matcher Throwable:" + e);
			e.printStackTrace();
		}
		// ----------------------------------------------------------------

		String json = new APIResponse(match, error).toJson();
		return Response.status(200).entity(json).build();
		// ----------------------------------------------------------------
	}
}