package com.infrrd.api.response;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * API Response
 * 
 * @author Vaibhav Pathare
 *
 */
public class APIResponse {
	private String match;
	private boolean error;

	public APIResponse(String match, boolean error) {
		this.match = match;
		this.error = error;
	}

	public String getMatch() {
		return match;
	}

	public void setMatch(String match) {
		this.match = match;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String toJson() {
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls()
				.create();
		return gson.toJson(this);
	}
}